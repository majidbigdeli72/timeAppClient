import m from "mithril"
import Login from "./views/Login";  
import Layout from "./views/Layout";
import Task from "./views/Task";


m.route(document.body, "/login", {
    "/login": {
        render: function() {
            return m(Login)
        }
    },
    "/task": {
        render: function() {
            return m(Layout,m(Task))
        }
    },

})