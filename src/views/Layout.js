var m = require("mithril");
import "../../content/css/layout.css";

export default function Layout(params) {
  return {
    view: (vnode) => (
      <div style="height:100vh">
        <aside>
          <nav class="nav-vertical">
            <div class="hamburger menu--active">
              <img
                src="./content/image/26275762.jfif"
                alt="Avatar"
                style="width:3.25em;border-radius: 50%;"
              />
            </div>
            <ul class="nav-list list__active">
              <li class="nav__item">
                <a href="#intro" class="nav__link c_red">
                  <i class="fa fa-home" aria-hidden="true"></i>
                </a>
              </li>
              <li class="nav__item">
                <a href="#intro" class="nav__link c_blue">
                  <i class="fa fa-code" aria-hidden="true"></i>
                </a>
              </li>
              <li class="nav__item">
                <a href="#intro" class="nav__link c_yellow">
                  <i class="fa fa-download" aria-hidden="true"></i>
                </a>
              </li>
              <li class="nav__item">
                <a href="#intro" class="nav__link c_green">
                  <i class="fa fa-envelope" aria-hidden="true"></i>
                </a>
              </li>
            </ul>
          </nav>
        </aside>
        <div style="padding-left:4.25em;">
          <header>
            <div style="height:70px;background:#27c26c;padding-top:10px;padding-left:20px">
              <h4 style="color:#fff;font-weight:600">
                Create UI For TimeDoctor App
              </h4>
              <strong style="color:#fff;font-weight:600">AloVoip</strong>
              <h4 style="color: rgb(255, 255, 255);font-weight: 600;font-size: 30px;position: absolute;top: 11px;right: 136px;">
                00:57:17
              </h4>
            </div>
            <div style="height:50px;line-height: 50px;;background:#fff;box-shadow: 0px 1px 8px #00000052;">
              <p style="color:#000;position: absolute;right: 135px;">
                Worked Today: <strong style="font-weight:600">3h 16m</strong>
              </p>
            </div>
            <button
              type="button"
              style="width: 60px;background:#eb4e29;color: #fff;font-size: 20px;;border: none;outline: none;box-shadow: 0px 1px 4px 3px #00000042;cursor: pointer;;height: 60px; border-radius: 50%;position: absolute;top: 39px; right: 56px;"
            >
              <i class="fa fa-pause" aria-hidden="true"></i>
            </button>
          </header>
          <main style="padding:15px;">{vnode.children}</main>
        </div>
      </div>
    ),
  };
}
